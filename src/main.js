var words
var wordsExtended
var keyCheck = [false, false]
var keys = [null, null]
var pos = [null, null]
var keyDist = null

function setup() {
  words = loadStrings('txt/en_EN.txt')
}

function checkInitialWords() {

  modal0 = document.getElementById("initialWordError")
  modal2 = document.getElementById("encryptionError")
  modal3 = document.getElementById("decryptionError")
  toEncrypt = document.getElementById("toEncrypt").value
  toDecrypt = document.getElementById("toDecrypt").value
  key1 = document.getElementById('key1').value
  key2 = document.getElementById('key2').value

  for (var i = 0; i < words.length - 1; i++) {
    if (key1 === words[i]) {
      keyCheck[0] = true
      pos[0] = i
    }
    if (key2 === words[i]) {
      keyCheck[1] = true
      pos[1] = i
    }
  }

  if (keyCheck[0] === false || keyCheck[1] === false) {
    modal0.style.display = "block"
    window.onclick = function(event) {
      if (event.target == modal0 || event.target == modal1 || event.target == modal2) {
        modal0.style.display = "none"
        modal1.style.display = "none"
        modal2.style.display = "none"
      }
    }
  }

  setTimeout(function() {
    keyCheck = [false, false]
  }, 100)

}

function encryption() {

  wordsExtended = words.concat(words)
  key1 = document.getElementById('key1').value
  key2 = document.getElementById('key2').value
  toEncrypt = document.getElementById('toEncrypt').value.toLowerCase()
  modal0 = document.getElementById("initialWordError")
  modal1 = document.getElementById("encryptionError")
  modal2 = document.getElementById("decryptionError")

  for (var i = 0; i < words.length - 1; i++) {
    if (key1 === words[i]) {
      keyCheck[0] = true
      pos[0] = i
    }
    if (key2 === words[i]) {
      keyCheck[1] = true
      pos[1] = i
    }
  }

  if (toEncrypt.length === 0) {
    modal1.style.display = "block"
    window.onclick = function(event) {
      if (event.target == modal0 || event.target == modal1 || event.target == modal2) {
        modal0.style.display = "none"
        modal1.style.display = "none"
        modal2.style.display = "none"
      }
    }
  }
  if (keyCheck[0] === false || keyCheck[1] === false) {
    modal0.style.display = "block"
    window.onclick = function(event) {
      if (event.target == modal0 || event.target == modal1 || event.target == modal2) {
        modal0.style.display = "none"
        modal1.style.display = "none"
        modal2.style.display = "none"
      }
    }
  } else {
    for (var i = 0; i < words.length - 1; i++) {
      if (key1 === words[i]) {
        pos[0] = i
      }
      if (key2 === words[i]) {
        pos[1] = i
      }
    }

    keyDist = pos[1] - pos[0]
    toEncryptSplitted = toEncrypt.split(" ")
    punctuations = []
    cipherTextSplitted = []
    for (var i = 0; i < toEncryptSplitted.length; i++) {
      punctuations.push('')
      cipherTextSplitted.push('')
    }
    for (var i = 0; i < toEncryptSplitted.length; i++) {
      ending = toEncryptSplitted[i][toEncryptSplitted[i].length - 1]
      if (ending === '.') {
        punctuations[i] = '.'
        toEncryptSplitted[i] = toEncryptSplitted[i].slice(0, -1)
      }
      if (ending === ',') {
        punctuations[i] = ','
        toEncryptSplitted[i] = toEncryptSplitted[i].slice(0, -1)
      }
      if (ending === '?') {
        punctuations[i] = '?'
        toEncryptSplitted[i] = toEncryptSplitted[i].slice(0, -1)
      }
      if (ending === '!') {
        punctuations[i] = '!'
        toEncryptSplitted[i] = toEncryptSplitted[i].slice(0, -1)
      }
      if (ending === ':') {
        punctuations[i] = '.'
        toEncryptSplitted[i] = toEncryptSplitted[i].slice(0, -1)
      }
      if (ending === ';') {
        punctuations[i] = ';'
        toEncryptSplitted[i] = toEncryptSplitted[i].slice(0, -1)
      }
    }
    for (var i = 0; i < toEncryptSplitted.length; i++) {
      cipherTextSplitted[i] = toEncryptSplitted[i]
    }

    for (var i = 0; i < toEncryptSplitted.length; i++) {
      for (var j = 0; j < words.length; j++) {
        if (words[j] === toEncryptSplitted[i]) {
          if (keyDist >= 0) {
            cipherTextSplitted[i] = words[(j + keyDist) % words.length]
          }
          if (keyDist < 0) {
            cipherTextSplitted[i] = wordsExtended[words.length + j + keyDist]
          }
        }
      }
    }

    cipherText = ''
    for (var i = 0; i < punctuations.length; i++) {
      punctuations[i] += ' '
      if (punctuations[i] === ". " || punctuations[i] === "? " || punctuations[i] === "! ") {
        if ((i + 1) < punctuations.length) {
          cipherTextSplitted[i + 1] = capitalizeFirstLetter(cipherTextSplitted[i + 1])
        }
      }
      cipherText += (cipherTextSplitted[i] + punctuations[i])
    }
    cipherText = cipherText.slice(0, -1)
    cipherText = capitalizeFirstLetter(cipherText)
    document.getElementById("toDecrypt").value = cipherText
  }

}

function decryption() {

  wordsExtended = words.concat(words)
  key1 = document.getElementById('key1').value
  key2 = document.getElementById('key2').value
  toDecrypt = document.getElementById('toDecrypt').value.toLowerCase()
  modal0 = document.getElementById("initialWordError")
  modal1 = document.getElementById("encryptionError")
  modal2 = document.getElementById("decryptionError")

  for (var i = 0; i < words.length - 1; i++) {
    if (key1 === words[i]) {
      keyCheck[0] = true
      pos[0] = i
    }
    if (key2 === words[i]) {
      keyCheck[1] = true
      pos[1] = i
    }
  }

  if (toDecrypt.length === 0) {
    modal2.style.display = "block"
    window.onclick = function(event) {
      if (event.target == modal0 || event.target == modal1 || event.target == modal2) {
        modal0.style.display = "none"
        modal1.style.display = "none"
        modal2.style.display = "none"
      }
    }
  }
  if (keyCheck[0] === false || keyCheck[1] === false) {
    modal0.style.display = "block"
    window.onclick = function(event) {
      if (event.target == modal0 || event.target == modal1 || event.target == modal2) {
        modal0.style.display = "none"
        modal1.style.display = "none"
        modal2.style.display = "none"
      }
    }
  } else {
    for (var i = 0; i < words.length - 1; i++) {
      if (key1 === words[i]) {
        pos[0] = i
      }
      if (key2 === words[i]) {
        pos[1] = i
      }
    }

    keyDist = pos[0] - pos[1]
    toDecryptSplitted = toDecrypt.split(" ")
    punctuations = []
    plainTextSplitted = []
    for (var i = 0; i < toDecryptSplitted.length; i++) {
      punctuations.push('')
      plainTextSplitted.push('')
    }
    for (var i = 0; i < toDecryptSplitted.length; i++) {
      ending = toDecryptSplitted[i][toDecryptSplitted[i].length - 1]
      if (ending === '.') {
        punctuations[i] = '.'
        toDecryptSplitted[i] = toDecryptSplitted[i].slice(0, -1)
      }
      if (ending === ',') {
        punctuations[i] = ','
        toDecryptSplitted[i] = toDecryptSplitted[i].slice(0, -1)
      }
      if (ending === '?') {
        punctuations[i] = '?'
        toDecryptSplitted[i] = toDecryptSplitted[i].slice(0, -1)
      }
      if (ending === '!') {
        punctuations[i] = '!'
        toDecryptSplitted[i] = toDecryptSplitted[i].slice(0, -1)
      }
      if (ending === ':') {
        punctuations[i] = '.'
        toDecryptSplitted[i] = toDecryptSplitted[i].slice(0, -1)
      }
      if (ending === ';') {
        punctuations[i] = ';'
        toDecryptSplitted[i] = toDecryptSplitted[i].slice(0, -1)
      }
    }
    for (var i = 0; i < toDecryptSplitted.length; i++) {
      plainTextSplitted[i] = toDecryptSplitted[i]
    }

    for (var i = 0; i < toDecryptSplitted.length; i++) {
      for (var j = 0; j < words.length; j++) {
        if (words[j] === toDecryptSplitted[i]) {
          if (keyDist >= 0) {
            plainTextSplitted[i] = words[(j + keyDist) % words.length]
          }
          if (keyDist < 0) {
            plainTextSplitted[i] = wordsExtended[words.length + j + keyDist]
          }
        }
      }
    }

    plainText = ''
    for (var i = 0; i < punctuations.length; i++) {
      punctuations[i] += ' '
      if (punctuations[i] === ". " || punctuations[i] === "? " || punctuations[i] === "! ") {
        if ((i + 1) < punctuations.length) {
          plainTextSplitted[i + 1] = capitalizeFirstLetter(plainTextSplitted[i + 1])
        }
      }
      plainText += (plainTextSplitted[i] + punctuations[i])
    }
    plainText = plainText.slice(0, -1)
    plainText = capitalizeFirstLetter(plainText)
    document.getElementById("toEncrypt").value = plainText
  }

}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}
